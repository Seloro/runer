﻿using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.Windows;

public class Controller_Player : MonoBehaviour
{
    float xInicial;
    bool acelerar, enEstatica;
    public GameObject escudo;
    private void Start()
    {
        xInicial = transform.position.x;
        escudo.gameObject.SetActive(false);
    }

    void Update()
    {
        GetInput();
        muertePorEstatica();
    }

    private void GetInput()// contiene todos los metodos de inputs
    {
        movimiento();
        aceleron();
        cambioColor();
        trucoEscudo();
    }

    private void movimiento()// movimiento vertical del jugador
    {
        if (UnityEngine.Input.GetKey(KeyCode.W) && transform.position.y < 15)
        {
            transform.position += Vector3.up * Incrementador.incremento / 100;

            if (transform.position.y > 15)
            {
                transform.position = new Vector3(transform.position.x, 15, transform.position.z);
            }
        }
        else if (UnityEngine.Input.GetKey(KeyCode.S) && transform.position.y > 0)
        {
            transform.position += Vector3.down * Incrementador.incremento / 100;

            if (transform.position.y < 0)
            {
                transform.position = new Vector3(transform.position.x, 0, transform.position.z);
            }
        }
    }

    private void aceleron()// mueve de forma horizontal al juagador X distancia y despues lo retrosede
    {
        if (UnityEngine.Input.GetKeyDown(KeyCode.D))
        {
            if (transform.position.x == xInicial)
            {
                acelerar = true;
            }
        }

        if (acelerar)
        {
            if (transform.position.x <= xInicial + 15)
            {
                transform.position += Vector3.right * Incrementador.incremento / 10;
            }
            else
            {
                acelerar = false;
            }
        }
        else
        {
            if (transform.position.x > xInicial)
            {
                transform.position -= Vector3.right * Incrementador.incremento / 1000;
            }
            else
            {
                transform.position = new Vector3(xInicial, transform.position.y, transform.position.z);
            }
        }
    }
    private void cambioColor()// cambia el color del jugador y su escudo cuando mantiene apretada la tecla A
    {
        if (UnityEngine.Input.GetKey(KeyCode.A))
        {
            GetComponent<Renderer>().material.color = Color.magenta;
            escudo.GetComponent<Renderer>().material.color = Color.magenta;
        }
        else
        {
            GetComponent<Renderer>().material.color = Color.blue;
            escudo.GetComponent<Renderer>().material.color = Color.cyan;
        }
    }
    private void trucoEscudo()// trampa para activar el power up escudo
    {
        if (UnityEngine.Input.GetKeyDown(KeyCode.E))
        {
            escudo.gameObject.SetActive(true);
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))// si entra en contacto con un objeto que tenca la etiqueta enemigo
        {
            if (!escudo.gameObject.activeInHierarchy)// y el escudo no esta activado pierde
            {
                Destroy(this.gameObject);
                Controller_Hud.gameOver = true;
            }
            else// sino destrulle al enemigo y desactiva el escudo
            {
                Destroy(collision.gameObject);
                escudo.gameObject.SetActive(false);
            }
        }
        if (collision.gameObject.CompareTag("Escudo"))// activa el escudo cuando entra en contacto con un objeto con la etiqueta Escudo
        {
            escudo.gameObject.SetActive(true);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Estatica"))// si entra en contacto con un objeto con evento triger, y este tiene la etiqueta estatica, vuelve verdadero el booleano enEstatica
        {
            enEstatica = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Estatica"))// si ya no esta en contacto con un objeto con evento triger, y este tenga la etiqueta estatica, vuelve falso el booleano enEstatica
        {
            enEstatica = false;
        }
    }
    private void muertePorEstatica()// si el jugador esta enEstatica y no es de color magenta pierde
    {
        if (enEstatica && GetComponent<Renderer>().material.color != Color.magenta)
        {
            Destroy(this.gameObject);
            Controller_Hud.gameOver = true;
        }
    }
}
