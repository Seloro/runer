﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    private float distance = 0;

    void Start()
    {
        gameOver = false;
        distance = 0;
        distanceText.text = distance.ToString();
        gameOverText.gameObject.SetActive(false);
    }

    void Update()
    {
        if (gameOver)// incrementa el contador de distancia mientras el jugador no alla perdido
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Total Distance: " + ((int)distance).ToString() + " m";
            gameOverText.gameObject.SetActive(true);
        }
        else
        {
            distance += Time.deltaTime * Incrementador.incremento;
            distanceText.text = ((int)distance).ToString() + " m";
        }
    }
}
