using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemio_Basico : MonoBehaviour
{
    public bool normal, vertical, muro, estatica;
    public int limiteD, reductorDeVelocidad;
    void Start()
    {
        inicio();
    }
    void Update()
    {
        movimiento();
        destructor();
    }
    private void movimiento()// mueve horizontalmente todos los enemigos, reductorDeVelocidad sirve para hacer que cada enemigo se mueva a una velocidad distinta
    {
        if (!Controller_Hud.gameOver)
        {
            transform.position += Vector3.left * Incrementador.incremento / reductorDeVelocidad;
        }
    }
    private void destructor()// destrulle al enemigo al alcanzar sierto limite de distancia (limiteD), en caso de ser enemigo muro o estatica vuelve falsos sus respectivos booleanos
    {
        if (this.transform.position.x <= -limiteD)
        {
            if (muro)
            {
                Controller_Instantiator.muroActivo = false;
            }
            if (estatica)
            {
                Controller_Instantiator.estaticaActiva = false;
            }
            Destroy(this.gameObject);
        }
    }
    private void inicio()// dependiendo del tipo de enemigo que sean camvia sus escala, posiciones iniciales en Y y sus rotaciones
    {
        if (normal)
        {
            transform.position = new Vector3(transform.position.x, UnityEngine.Random.Range(0, 16), transform.position.z);
            transform.localScale = new Vector3(UnityEngine.Random.Range(1, 5), UnityEngine.Random.Range(1, 5), transform.localScale.z);
            transform.eulerAngles = Vector3.forward * UnityEngine.Random.Range(0, 181);
        }
        else if (vertical)
        {
            transform.position = new Vector3(transform.position.x, UnityEngine.Random.Range(0, 16), transform.position.z);
            transform.localScale = new Vector3(UnityEngine.Random.Range(1, 5), UnityEngine.Random.Range(1, 3), transform.localScale.z);
            transform.eulerAngles = Vector3.forward * UnityEngine.Random.Range(0, 181);
        }
        else if (estatica)
        {
            transform.localScale += Vector3.right * UnityEngine.Random.Range(5, 21);
            transform.position += Vector3.right * 10;
        }
    }
}
