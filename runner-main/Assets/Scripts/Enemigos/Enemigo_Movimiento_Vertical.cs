using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo_Movimiento_Vertical : MonoBehaviour
{
    int signo;
    void Start()
    {
        signo = UnityEngine.Random.Range(0, 2);// determina un signo de forma aleatoria

        if (signo == 0)// si es 0 cambia el signo a -1
        {
            signo = -1;
        }
    }
    void Update()
    {
        if (!Controller_Hud.gameOver)// se mueve de forma vertical si el jugador no perdio
        {
            transform.position += Vector3.up * signo * Incrementador.incremento / 100;
        }

        if (transform.position.y >= 15)// cambia los signos de pendiendo de su posicion en Y
        {
            signo = -1;
        }
        else if (transform.position.y <= 0)
        {
            signo = 1;
        }
    }

    private void OnCollisionEnter(Collision collision)// cambia de signo si entra en contacto con otro enemigo, cambia al signo contrario al que se encuentre
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (signo == 1)
            {
                signo = -1;
            }
            else
            {
                signo = 1;
            }
        }
    }
}
