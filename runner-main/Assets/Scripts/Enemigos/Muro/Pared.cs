using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pared : MonoBehaviour
{
    static public bool llaveActiva;
    int signo;
    void Start()
    {
        llaveActiva = false;
        signo = UnityEngine.Random.Range(0, 2);// establese un signo "aleatorio"

        if (signo == 0)// en caso de que sea sero se pasa a -1
        {
            signo = -1;
        }
    }
    void Update()
    {
        if (llaveActiva)// si se colisiono con la llave se empiesa a mover hacia arriba o hacia abajo, depende de signo
        {
            transform.position += Vector3.up * signo * Incrementador.incremento / 50;
        }
    }
}
