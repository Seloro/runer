using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Llave : MonoBehaviour
{
    private void Start()
    {
        transform.position = new Vector3(transform.position.x, UnityEngine.Random.Range(0, 16), transform.position.z);// se posiciona en una Y aleatoria
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Jugador"))// si entra en contacto con el jugador vuelve verdadero el booleano de pared.llaveActiva y se destrulle
        {
            Pared.llaveActiva = true;
            Destroy(gameObject);
        }
    }
}
