﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public GameObject instantiatePos;
    public float respawningTimer = 0;
    public GameObject activadorEscudo, muro, estatica;
    float distancia;
    float tempMuro, tempMuroMax, tempEstatica, tempEstaticaMax;
    static public bool muroActivo, estaticaActiva;
    private void Start()
    {
        tempMuroMax = UnityEngine.Random.Range(5, 11);
        tempEstaticaMax = 200;
        muroActivo = false;
    }

    void Update()
    {
        SpawnEnemies();
        crearActivadorEscudo();
        creaMuros();
        creaEstatica();
    }

    private void SpawnEnemies()// genera enemigos cada sierto tiempo (enemigo normal y vertical)
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(2, 6);
            respawningTimer += Incrementador.enfriamiento;
        }
    }

    private void crearActivadorEscudo()// genera un escudo al recorer 100 m de distancia
    {
        if (distancia >= 100)
        {
            distancia = 0;
            Instantiate(activadorEscudo, instantiatePos.transform);
        }
        else
        {
            distancia += Time.deltaTime * Incrementador.incremento;
        }
    }
    private void creaMuros()// crea al enemigo muro si no se encuentra uno en esena, se genera cada sierto tiempo
    {
        if (!muroActivo)
        {
            if (tempMuro >= tempMuroMax)
            {
                Instantiate(muro, instantiatePos.transform);
                tempMuro = 0;
                tempMuroMax = UnityEngine.Random.Range(5, 11);
                muroActivo = true;
            }
            else
            {
                tempMuro += Time.deltaTime;
            }
        }
    }
    private void creaEstatica()// genera al enemigo estatica a partir de recorer X distancia, la primera ves es a partir de los 200 (tempEstaticaMax en el start)
    {
        if (!estaticaActiva)
        {
            if (tempEstatica >= tempEstaticaMax)
            {
                Instantiate(estatica, instantiatePos.transform);
                tempEstatica = 0;
                tempEstaticaMax = UnityEngine.Random.Range(100, 150);
                estaticaActiva = true;
            }
            else
            {
                tempEstatica += Time.deltaTime * Incrementador.incremento;
            }
        }
    }
}
