using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Incrementador : MonoBehaviour
{
    private float time;
    static public float incremento, enfriamiento;

    private void Start()
    {
        incrementadorYreductor();
    }

    void Update()
    {
        incrementadorYreductor();
    }

    private void incrementadorYreductor()// genera dos numeros que van canviando con el tiempo desde el primero al segundo de forma progresiba
    {
        time += Time.deltaTime;
        incremento = Mathf.SmoothStep(1f, 5f, time / 40f);// se usa para aumentar la velosidad de jugador, enemigos,parallax, contador de distancia, ente otras cosas
        enfriamiento = Mathf.SmoothStep(5f, -2f, time / 40f);// se le suma al temporisador de los enemigos normal y vertical
    }
}
