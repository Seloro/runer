using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activador_Escudo : MonoBehaviour
{
    int indice, signo;
    bool vertical;
    private void Start()
    {
        transform.position = new Vector3(transform.position.x, UnityEngine.Random.Range(0, 16), transform.position.z);// se posiciona en una Y aleatoria
        indice = UnityEngine.Random.Range(0, 2);// se determina de forma aleatoria si el objeto se movera de forma vertical

        if (indice == 1)
        {
            vertical = true;
            indice = UnityEngine.Random.Range(0, 2);// se determina de forma aleatoria el signo + o - de signo

            if (indice == 1)
            {
                signo = 1;
            }
            else
            {
                signo = -1;
            }
        }
    }
    void Update()
    {
        movimiento();
        movimientoVertical();
        destructor();
    }
    private void movimiento()// movimiento horizontal
    {
        if (!Controller_Hud.gameOver)
        {
            transform.position += Vector3.left * Incrementador.incremento / 200;
        }
    }
    private void movimientoVertical()// si en el estar se determino que se mueve de forma vertical mueve el objeto de forma vertial dependiendo del signo
    {
        if (!Controller_Hud.gameOver && vertical)
        {
            transform.position += Vector3.up * signo * Incrementador.incremento / 200;
        }

        if (transform.position.y >= 15)// cambia los snodependiendo de la altura en la que se encuentre
        {
            signo = -1;
        }
        else if (transform.position.y <= 0)
        {
            signo = 1;
        }
    }
    private void destructor()// destrulle el objeto si tiene un valor en X menor a -30
    {
        if (this.transform.position.x <= -30)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter(Collision collision)// destrulle el objeto si entra en colicion con otro
    {
        Destroy(this.gameObject);
    }
}
