﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject cam;
    private float length, startPos;

    void Start()
    {
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {
        if (!Controller_Hud.gameOver)// mueve las imajenes de fondo si el juegor no perdio, tamvien las repocisiona cuando se encuentra en una X menor a -20
        {
            transform.position = new Vector3(transform.position.x - Incrementador.incremento / 10, transform.position.y, transform.position.z);
            if (transform.localPosition.x < -20)
            {
                transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z);
            }
        }
    }
}
